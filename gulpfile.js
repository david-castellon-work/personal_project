const	path		= require ( 'path' ) ;

const	DEV_MODE	= ( process . env . NODE_ENV !== 'production' ) ,
	SRC_FILE	= path . resolve ( 'src' ) ,
	NODE_MOD	= path . resolve ( 'node_modules' ) ,
	BUILD_FILE	= path . resolve ( 'build' ) ;

const	VIEW_SRC	= path . join ( SRC_FILE , 'views/**/*.pug' ) ,
	VIEW_OUT	= path . join ( BUILD_FILE , 'views' ) ,

	STYLE_SRC	= path . join ( SRC_FILE , 'public/styles/notes.scss' ) ,
	STYLE_OUT	= path . join ( BUILD_FILE , 'public/styles' ) ,

	DOCUMENT_SRC	= path . join ( SRC_FILE , 'public/documents/**/*' ) ,
	DOCUMENT_OUT	= path . join ( BUILD_FILE , 'public/documents/' ) ,

	CLIENT_SCRIPT_SRC	= path . join ( SRC_FILE , 'public/scripts/notes.jsx' ) ,
	CLIENT_SCRIPT_OUT	= path . join ( BUILD_FILE , 'public/scripts' ) ,

	SERVER_SCRIPT_SRC	= path . join ( SRC_FILE , '**/*.js' ) ,
	SERVER_SCRIPT_OUT	= path . join ( BUILD_FILE , '**/*.js' ) ;


const	gulp		= require ( 'gulp' ) ,
	nodemon		= require ( 'gulp-nodemon' ) ,
	browserSync	= require ( 'browser-sync' ) ,
	newer		= require ( 'gulp-newer' ) ,
	noop		= require ( 'gulp-noop' ) ,
	sourcemaps	= ( DEV_MODE ? require ( 'gulp-sourcemaps' ) : null ) ,
	inject		= require ( 'gulp-inject' ) ,
	browserify	=require ( 'browserify' ) ,
	sourceStream	= require ( 'vinyl-source-stream' ) ,
	buffer			= require ( 'vinyl-buffer' ) ,

	babel		= require ( 'gulp-babel' ) ,
	terser		= require ( 'gulp-terser' ) ,
	stripdebug	= ( DEV_MODE ? null : require ( 'gulp-strip-debug' ) ) ,

	sass		= require ( 'gulp-sass' ) (require('node-sass')),
	postcss		= require ( 'gulp-postcss' ) ,
	cssnano		= require ( 'cssnano' ) ;

gulp . task ( 'views' , views ) ;
gulp . task ( 'styles' , styles ) ;
gulp . task ( 'public-documents' , publicDocuments ) ;
gulp . task ( 'client-scripts' , clientScripts ) ;
gulp . task ( 'scripts' , scripts ) ;
gulp . task ( 'watch' , watch ) ;
gulp . task ( 'server' , start_nodemon ) ;
gulp . task ( 'default' , gulp . series ( gulp . parallel ( 'styles' , 'scripts' , 'public-documents' ) , 'views' ) ) ;
gulp . task ( 'dev' , gulp . series ( 'default' , 'server' , 'watch' ) ) ;

function views ( ) {
	const sources = gulp . src (
		path . join ( STYLE_OUT , '**/*.css' ) ,
		{ read : false } ,
		{ relative : true }
	) ;
	return gulp . src ( VIEW_SRC )
		. pipe ( newer ( VIEW_OUT ) )
		. pipe ( inject ( sources , {
			ignorePath : 'build' ,
			addPrefix : '/personalsite' ,
		} ) )
		. pipe ( gulp . dest ( path . join ( BUILD_FILE , 'views' ) ) ) ;
}
function styles ( ) {
	return gulp . src ( STYLE_SRC )
		. pipe ( newer ( STYLE_OUT ) )
		. pipe ( sourcemaps ? sourcemaps . init ( ) : noop ( ) )

		. pipe ( sass ( ) . on ( 'error' , sass . logError ) )
		. pipe ( postcss ( [ cssnano ] ) )
		. pipe ( sourcemaps ? sourcemaps . write ( './maps' ) : noop ( ) )
		. pipe ( gulp . dest ( STYLE_OUT ) ) ;
}
function publicDocuments ( ) {
	return gulp . src ( DOCUMENT_SRC )
		. pipe ( gulp . dest ( DOCUMENT_OUT ) ) ;
}
function clientScripts ( ) {
	browserify ( {
		entries : CLIENT_SCRIPT_SRC ,
		fullPaths : true ,
		debug : true ,
		sourceMaps : true ,
		cache : { } ,
		packageCache : { } ,
	} )
	. transform ( 'babelify' , {
		global : true ,
		// transform all files that are imported
		// only : [ /\/node_modules\/bootstrap\// ] ,
		// do not transform the files that match with the regex
		// the regex is all files except bootstrap. so it should
		// ignore everything except bootstrap
		// src/components/password-toggle/password-toggle.js
		ignore : [ /\/node_modules\/(?!bootstrap)\// , ] ,
		// ignore : [ /node_modules\/(?!bootstrap)/ ] ,
		// ignore : [ /node_modules/ ] ,
		// ignore : [ 'src/dark.js' , 'src/test.js' ] ,
		// ignore : [ /(?!\/src\/dark.js)|(\/node_modules\/(?!bootstrap))/ ] ,
		presets : [
			[
				'@babel/env' ,
				{
					loose : true ,
					// modules : false ,
					exclude: [ 'transform-typeof-symbol' ] ,
				}
			] ,
		] ,
		plugins : [
			'@babel/plugin-proposal-object-rest-spread' ,
		] ,
	} )
	. bundle ( )
	. on ( 'error' , err => console.log ( 'Browserify Error: ' + err.message ) )
	. pipe ( sourceStream( SCRIPTS_OUT ) )
	. pipe ( buffer( ) )
	. pipe ( sourcemaps . init ( { loadMaps : true } ) )
	. pipe ( terser ( ) )
	. pipe ( sourcemaps . write ( './maps' ) )
	. pipe ( gulp.dest ( CLIENT_SCRIPT_OUT ) ) ;
}
function scripts ( ) {
	return gulp . src ( SERVER_SCRIPT_SRC )
		. pipe ( newer ( BUILD_FILE ) )
		. pipe ( sourcemaps ? sourcemaps . init ( ) : noop ( ) )
		. pipe ( babel ( {
			presets : [ '@babel/preset-env' ]
		} ) )
		. pipe ( terser ( ) )
		. pipe ( stripdebug ? stripdebug ( ) : noop ( ) )
		. pipe ( sourcemaps ? sourcemaps . mapSources ( function ( sourcePath , file ) {
			return path . join ( SRC_FILE , sourcePath ) ;
		} ) : noop ( ) )
		. pipe ( sourcemaps ? sourcemaps . write ( './maps' , {
			includeContent : false
		} ) : noop ( ) )
		. pipe ( gulp . dest ( BUILD_FILE ) ) ;
}
// updates build on changes
function watch ( done ) {
	gulp . watch ( VIEW_SRC , gulp . series ( views ) ) ;
	gulp . watch ( STYLE_SRC , styles ) ;
	gulp . watch ( SERVER_SCRIPT_SRC , scripts ) ;
	done ( ) ;
}
// restarts server on changes
function start_nodemon ( done ) {
	const STARTUP_TIMEOUT = 5000 ;

	const server = nodemon ( {
		script : path . join ( BUILD_FILE , 'server.js' ) ,
		stdout : false ,
		env : { 'NODE_ENV' : 'development' , 'PORT' : '3000' } ,
	} ) ;
	let starting = false ;

	const onReady = function ( ) {
		starting = false ;
		done ( ) ;
	} ;
	server . on ( 'start' , function ( ) {
		starting = true ;
		setTimeout ( onReady , STARTUP_TIMEOUT ) ;
	} ) ;

	server . on ( 'stdout' , function ( stdout ) {
		process . stdout . write ( stdout ) ;
		if ( starting ) { onReady ( ) ; }
	} ) ;
}
function start_browser_sync_public ( done ) {
	browserSync . create ( ) . init ( {
		proxy : 'http://localhost:3000' ,
		files : [ path . join ( BUILD_FILE , 'public' ) , path . join ( BUILD_FILE , 'views' ) ] ,
		browser : 'google chrome firefox' ,
		port : 4000
	} , done ) ;
} 
