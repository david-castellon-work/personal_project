import express from 'express' ;
import app from './app' ;
import morgan from 'morgan' ;

import intuitive from 'intuitive/build/app' ;
import dark from 'dark/build/app' ;
import samplelibrary from 'samplelibrary/build/app'	;
import toy from 'toy/build/app' ;

var server = express ( ) ;
const port = process . env . PORT || '3000' ;

server . use ( morgan ( 'tiny' ) ) ;

server . use ( '/personalsite' , app ) ;
server . use ( '/intuitive' , intuitive ) ;
server . use ( '/dark' , dark ) ;
server . use ( '/samplelibrary' , samplelibrary ) ;
server . use ( '/toy' , toy ) ;
server . get ( '/' , personalsite_redirect ) ;
server . get ( '/*' , personalsite_redirect ) ;

server . listen ( port ) ;

function personalsite_redirect ( req , res ) {
	res . redirect ( '/personalsite' ) ;
}
