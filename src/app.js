import express			from 'express'			;
import path			from 'path'			;

import mongoose			from 'mongoose'			;

import session			from 'express-session'		;
import bodyParser		from 'body-parser'		;
import cookieParser		from 'cookie-parser'		;
import multer			from 'multer'			;

import routes			from './routes/routes' ;

const	app 		= express ( )			,
	upload		= multer ( )			,
	BUILD_FILE	= __dirname 			;

console.log("build file location: BUILD_FILE");

app . set ( 'view engine' , 'pug' ) ;
app . set ( 'views' , path . join ( BUILD_FILE , 'views' ) ) ; 
// static assets
app . use ( '/public' , express . static ( path . join ( BUILD_FILE , 'public' ) ) ) ;
app . use ( '/' , routes ) ;

app . use ( express . urlencoded ( { extended : false } ) ) ;
app . use ( express . json ( ) ) ;
app . use ( upload . array ( ) ) ;


export default app ;
