import express from 'express' ;

var router = express . Router ( ) ;

router . get ( '/' , ( req , res ) => {
	res . render ( 'homepage' ) ;
} ) ;

export default router ;
