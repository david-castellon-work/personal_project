import express from 'express' ;
const router = express . Router ( ) ;

import homepageRouter		from './homepage' ;
import resumeRouter		from './resume' ;

const homepage			= '/homepage'		;
const resume			= '/resume'		;

router . use ( '/homepage' , homepageRouter	) ;
router . use ( '/resume' , resumeRouter		) ;

router . use ( '/' , function ( req , res ) {
 	res . redirect ( '/personalsite/homepage' ) ;
} ) ;

export default router ;
