# Personal Site

Place where I put stuff I'm working on.

## Directories

src: Contains my source files for my app.
build: Contains files to be directly executed in node.

## NPM Commands:

build-watch: Builds in watch mode.
test: Builds the server, then tests.
prod-build: Builds the production build.
prod-start: Starts production build in production mode on port 80.
